你可以通过以下方式反馈问题：

* [github :issue](https://github.com/ihewro/typecho-theme-handsome/issues)
* [主题交流群：611102614]()
* [邮件交流：ihewro#163.com](mailto:ihewro@163.com)
