### 安装

1. 下载主题文件包并解压后，重命名主题文件夹为`handsome`。

2. 目录下有两个文件夹。`handsome`和`插件`。

3. 上传`插件`下的所有文件至typecho的`/usr/plugins/`目录下，上传完毕后可以删除`插件`文件夹。

4. 上传`handsome`文件夹至typecho的`/usr/themes`目录下。

5. 后台启用`handsome`主题。



### 更新

一般有两种方法：

1. 下载后，覆盖`/usr/themes/handsome/`目录即可。

2. 先删除`/usr/themes/handsome/`目录中的内容，然后新主题中的内容复制到`/usr/themes/handsome/`目录。（推荐√）

**\*每次更新会有更新提示，会提示是否有必要删除旧版本的主题文件夹。**
